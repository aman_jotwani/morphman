import pygame, sys
from pygame.locals import *
import random
from gameobjects import Coin, Hook, CoffeeBean, Morpho

class Game:
	def __init__(self, window):
		self.window = window
		self.font = pygame.font.Font(None, 35)

		self.timer = pygame.time.Clock()
		pygame.mixer.music.load("endrit_tone.mp3")
		pygame.mixer.music.play(loops=3)
		self.victory_sound = pygame.mixer.Sound("victory.wav")

	def startup(self):
		self.points = 0

		self.player = Morpho(300, 300)
		self.coin = Coin(450, 500)
		self.hook1 = Hook((0, 200), (400, 200))
		self.hook2 = Hook((100, 0), (100, 300))
		self.bean = CoffeeBean(-10, -10)

		self.coin_grp = pygame.sprite.GroupSingle(self.coin)
		self.bean_grp = pygame.sprite.GroupSingle(self.bean)
		self.hook_grp = pygame.sprite.Group(self.hook1, self.hook2)
		self.player_grp = pygame.sprite.GroupSingle(self.player)

	def run(self):
		self.startup()
		self.start_screen()
		self.game_loop()

	def start_screen(self):
		# remove magic colors
		# title
		title = self.font.render("MOCHA HIGH", True, (0, 255, 255))
		title_rect = Rect(300 - title.get_width()/2,
						  50,
						  title.get_width(),
						  title.get_height())

		# space key and its description
		space_key = self.font.render("Space ----- ", False, (255, 0, 0))
		space_key_rect = Rect(100, 
							  100,
							  space_key.get_width(),
							  space_key.get_height())

		space_descr = self.font.render("Have mocha", False, (255, 0, 0))
		space_descr_rect = Rect(100 + space_key.get_width() + 10,
								100,
								space_descr.get_width(),
								space_descr.get_height())

		# arrow keys and their description
		arrow_keys = self.font.render("Arrow keys ----- ", False, (255, 0, 0))
		arrow_keys_rect = Rect(100,
							   200,
							   arrow_keys.get_width(),
							   arrow_keys.get_height())
		arrow_descr = self.font.render("Movement", False, (255, 0, 0))
		arrow_descr_rect = Rect(100 + arrow_keys.get_width() + 10,
								200,
								arrow_descr.get_width(),
								arrow_descr.get_height())

		s_key = self.font.render("S -----", False, (255, 0, 0))
		s_key_rect = Rect(100,
						  300,
						  s_key.get_width(),
						  s_key.get_height())
		s_descr = self.font.render("Stop", False, (255, 0, 0))
		s_descr_rect = Rect(100 + s_key.get_width() + 10,
							300,
							s_descr.get_width(),
							s_descr.get_height())

		instructions1 = self.font.render("Collect coins in a room filled",
										False,
										(255, 0, 0))
		instructions1_rect = Rect(100,
								 400,
								 instructions1.get_width(),
								 instructions1.get_height())

		instructions2 = self.font.render("with fast-moving hooks.", False, (255, 0, 0))
		instructions2_rect = Rect(100,
								  400 + instructions1.get_height(),
								  instructions2.get_width(),
								  instructions2.get_height())
		done = False
		while not done:
			for event in pygame.event.get():
				if event.type == KEYDOWN and event.key == K_ESCAPE \
				or event.type == pygame.QUIT:
					sys.exit()
				elif event.type == KEYDOWN and event.key == K_SPACE:
					done = True
			self.window.fill((0, 175, 175))
			pygame.Surface.blit(self.window, space_key, space_key_rect)
			pygame.Surface.blit(self.window, space_descr, space_descr_rect)
			pygame.Surface.blit(self.window, arrow_keys, arrow_keys_rect)
			pygame.Surface.blit(self.window, arrow_descr, arrow_descr_rect)
			pygame.Surface.blit(self.window, title, title_rect)
			pygame.Surface.blit(self.window, s_key, s_key_rect)
			pygame.Surface.blit(self.window, s_descr, s_descr_rect)
			pygame.Surface.blit(self.window, instructions1, instructions1_rect)
			pygame.Surface.blit(self.window, instructions2, instructions2_rect)
			pygame.display.flip()

	def win_screen(self):
		pygame.mixer.music.stop()
		self.victory_sound.play()

		you_won = self.font.render("You won! :-D Good job!", False, (0, 255, 0))
		you_won_rect = Rect(300 - you_won.get_width()/2,
							300,
							you_won.get_width(),
							you_won.get_height())
		done = False
		while not done:
			for event in pygame.event.get():
				if event.type == KEYDOWN and event.key == K_ESCAPE \
				or event.type == pygame.QUIT:
					sys.exit()
			self.window.fill((0, 0, 0))
			pygame.Surface.blit(self.window, you_won, you_won_rect)
			pygame.display.flip()

	def end_screen(self):
		pygame.time.delay(1000)
		game_over = self.font.render("GAME OVER!", False, (0, 255, 0))
		game_over_rect = Rect(300 - game_over.get_width()/2,
							  50, 
							  game_over.get_width(),
							  game_over.get_height())
		restart = self.font.render("MOCHA IS THE FUTURE!", False, (255, 0, 0))
		restart_rect = Rect(300 - restart.get_width()/2,
							350, 
							restart.get_width(),
							restart.get_height())
		exit = self.font.render("SCREW THIS, I LIKE LATTE!", False, (0, 0, 255))
		exit_rect = Rect(300 - exit.get_width()/2,
						 400,
						 exit.get_width(),
						 exit.get_height())

		done = False
		while not done:
			for event in pygame.event.get():
				if event.type == KEYDOWN and event.key == K_ESCAPE \
				or event.type == pygame.QUIT:
					sys.exit()
				if event.type == MOUSEBUTTONDOWN and event.button == 1:
					if restart_rect.collidepoint((event.pos[0], event.pos[1])):
						done = True
					elif exit_rect.collidepoint((event.pos[0], event.pos[1])):
						sys.exit()
			self.window.fill((0, 0, 0))
			self.make_outline(restart_rect)
			self.make_outline(exit_rect)
			pygame.Surface.blit(self.window, game_over, game_over_rect)
			pygame.Surface.blit(self.window, restart, restart_rect)
			pygame.Surface.blit(self.window, exit, exit_rect)
			pygame.display.flip()
		self.run()

	def make_outline(self, content):
		if content.collidepoint(pygame.mouse.get_pos()):
			outline = content.copy()
			outline.topleft = (outline.topleft[0] - 2, outline.topleft[1] - 2)
			outline.width += 4
			outline.height += 4
			pygame.draw.rect(self.window, (0, 175, 175), outline, 1)

	def game_loop(self):
		while True:
			if not self.coin_grp.has(self.coin):
				self.points += 1
				if self.points == 20:
					self.win_screen()
				else:
					self.coin = Coin(random.randrange(50, 550, 150), random.randrange(50, 550, 150))
					self.coin_grp.sprite = self.coin
			if not self.hook_grp.has(self.hook1):
				target_y = random.randrange(50, 600, 50)
				self.hook1 = Hook((random.choice((0, 600)), target_y), (300, target_y))
				self.hook_grp.add(self.hook1)
			if not self.hook_grp.has(self.hook2):
				target_x = random.randrange(50, 600, 50)
				self.hook2 = Hook((target_x, random.choice((0, 600))), (target_x, 300))
				self.hook_grp.add(self.hook2)
			if not self.player_grp.has(self.player):
				self.end_screen()
			for event in pygame.event.get():
				if event.type == KEYDOWN and event.key == K_ESCAPE \
				or event.type == pygame.QUIT:
					sys.exit()
				else:
					self.player.handle_events(event)
					self.coin.handle_events(event)
			self.timer.tick(60)
			self.player_grp.update(self.coin, self.hook1, self.hook2, self.bean)
			self.coin_grp.update()
			self.hook_grp.update()
			window.fill((0, 175, 175))
			self.coin_grp.draw(window)
			if self.points >= 10:
				self.bean.reinit_bean(self.coin.rect)
				self.bean_grp.sprite = self.bean
				self.bean_grp.draw(window)
			self.hook_grp.draw(window)
			self.player_grp.draw(window)
			pygame.display.flip()

pygame.init()
size = (600, 600)
window = pygame.display.set_mode(size)
foo = Game(window)
foo.run()
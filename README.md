My pyweek 19 entry. Go here for more details - https://pyweek.org/e/Moch/

Requirements - python, pygame. Tested on windows. Download zip and run python morph.py to play.

Art credits - opengameart.org and Anay Kshirsagar.
Sound and music credits - opengameart.org(background music, death and victory sound effects) and freesound.org (getting coin)
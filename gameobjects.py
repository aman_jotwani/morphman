import pygame
from pygame.locals import *
import random

class Coin(pygame.sprite.DirtySprite):
	def __init__(self, x, y):
		pygame.sprite.DirtySprite.__init__(self)
		self.images = [
			"coin_01.png",
			"coin_02.png",
			"coin_03.png",
			"coin_04.png",
			"coin_05.png",
			"coin_06.png"
		]
		self.cursor = 0
		self.dirty = 2
		self.image = pygame.image.load(self.images[self.cursor]).convert()
		self.image.set_colorkey((0, 0, 0))
		self.rect = self.image.get_rect()
		self.rect.center = (x, y)
		self.x = self.rect.topleft[0]
		self.y = self.rect.topleft[1]
		self.occupied = False
		self.adjust_anim = False
		self.sound_effect = pygame.mixer.Sound("242857__plasterbrain__coin-get.ogg")

		# anim event
		pygame.time.set_timer(USEREVENT, 150)

	def handle_events(self, event):
		if event.type == USEREVENT:
			self.cursor += 1

	def update(self):
		if self.adjust_anim:
			self.rect.topleft = (self.x - 7, self.y)
			self.adjust_anim = False
		if self.cursor == 5:
			self.cursor = 0
		if self.cursor == 3:
			self.rect.topleft = (self.x + 7, self.y)
			self.adjust_anim = True
		self.image = pygame.image.load(self.images[self.cursor])

	def eat_coin(self):
		self.sound_effect.play()
		self.kill()

class Hook(pygame.sprite.Sprite):
	def __init__(self, start, target):
		pygame.sprite.Sprite.__init__(self)
		self.x = start[0]
		self.y = start[1]
		self.target_x = target[0]
		self.target_y = target[1]
		self.move_speed = 7
		self.done = False
		self.reached_target = False
		self.images = {
			"RIGHT" : "hook_right.png",
			"LEFT" : "hook_left.png",
			"UP" : "hook_up.png",
			"DOWN" : "hook_down.png"
		}
		if self.target_x > self.x:
			self.x_sign = 1
			self.image = pygame.image.load(self.images["RIGHT"]).convert()
		elif self.target_x < self.x:
			self.x_sign = -1
			self.image = pygame.image.load(self.images["LEFT"]).convert()
		else:
			self.x_sign = 0
		if self.target_y > self.y:
			self.y_sign = 1
			self.image = pygame.image.load(self.images["DOWN"]).convert()
		elif self.target_y < self.y:
			self.y_sign = -1
			self.image = pygame.image.load(self.images["UP"]).convert()
		else:
			self.y_sign = 0
		self.image.set_colorkey((255, 255, 255))
		self.rect = self.image.get_rect()
		self.rect.center = start

	def update(self):
		if not self.done:
			self.x += self.x_sign*self.move_speed
			self.y += self.y_sign*self.move_speed

			if self.x > self.target_x - self.rect.width/2 and self.x < self.target_x + self.rect.width/2 \
			and self.y > self.target_y - self.rect.height/2 and self.y < self.target_y + self.rect.width/2:
				self.x_sign = -self.x_sign
				self.y_sign = -self.y_sign
				self.reached_target = True

			if self.reached_target:
				if not pygame.display.get_surface().get_rect().collidepoint((self.x, self.y)):
					self.done = True
			self.rect.center = (self.x, self.y)
		else:
			self.kill()

class CoffeeBean(pygame.sprite.Sprite):
	def __init__(self, x, y):
		pygame.sprite.Sprite.__init__(self)
		self.image = pygame.image.load("bean.png").convert()
		self.image.set_colorkey((255, 255, 255))
		self.rect = self.image.get_rect()
		self.rect.topleft = (x, y)
		self.powerup_sound = pygame.mixer.Sound("")

	def consume_bean(self):
		self.powerup_sound.play()
		self.kill()

	def reinit_bean(self, coin_rect):
		if self.rect.colliderect(coin_rect) or \
		(self.rect.topleft == (-10, -10)):
			new_x = random.randrange(50, 550, 50)
			new_y = random.randrange(50, 550, 50)
			self.rect.topleft = (new_x, new_y)
			self.reinit_bean(coin_rect)
		else:
			return

class Morpho(pygame.sprite.Sprite):
	def __init__(self, x, y):
		pygame.sprite.Sprite.__init__(self)
		self.image = pygame.image.load("morph.png").convert()
		self.image.set_colorkey((255, 255, 255))
		self.rect = self.image.get_rect()
		self.rect.center = (x, y)
		self.velx = 0
		self.vely = 0
		self.speed = (self.velx, self.vely)
		self.MOVE_SPEED = 4
		self.death_sound = pygame.mixer.Sound("death.wav")
		self.is_flash = False
		self.STOP_FLASH_EVENT = USEREVENT + 5
		self.consumed_bean = False

	def handle_events(self, event):
		if event.type == KEYDOWN  and event.key == K_DOWN:
			self.vely = self.MOVE_SPEED
			self.velx = 0
		if event.type == KEYDOWN and event.key == K_UP:
			self.vely = -self.MOVE_SPEED
			self.velx = 0
		if event.type == KEYDOWN and event.key == K_RIGHT:
			self.velx = self.MOVE_SPEED
			self.vely = 0
		if event.type == KEYDOWN and event.key == K_LEFT:
			self.velx = -self.MOVE_SPEED
			self.vely = 0
		if event.type == KEYDOWN and event.key == K_SPACE and not self.is_flash:
			self.flash_mode()
		if event.type == KEYDOWN and event.key == K_s and not self.is_flash:
			self.velx = 0
			self.vely = 0
		if event.type == self.STOP_FLASH_EVENT:
			if self.consumed_bean:
				self.MOVE_SPEED = 10
			else:
				self.MOVE_SPEED = 4
			print 'stopping flash'
			self.set_speed()
			self.is_flash = False
			pygame.time.set_timer(self.STOP_FLASH_EVENT, 0)
			print 'stopped'

	def update(self, coin, hook1, hook2, bean):
		self.speed = (self.velx, self.vely)
		self.rect.move_ip(self.speed)
		if self.rect.colliderect(coin.rect):
			coin.eat_coin()
		if self.rect.colliderect(hook1.rect) \
		or self.rect.colliderect(hook2.rect):
			self.death_sound.play()
			self.kill()
		if self.rect.colliderect(bean):
			self.consumed_bean = True
			bean.consume_bean()
			self.MOVE_SPEED = 10
			self.set_speed()
		if self.rect.topleft[0] < 0 or self.rect.topleft[0] > 600 \
		or self.rect.topleft[1] < 0 or self.rect.topleft[1] > 600:
			self.death_sound.play()
			self.kill()

	def set_speed(self):
		print self.velx, self.vely
		if self.velx == 0:
			if self.vely > 0:
				self.vely = self.MOVE_SPEED
			elif self.vely < 0:
				self.vely = -self.MOVE_SPEED
		elif self.vely == 0:
			if self.velx > 0:
				self.velx = self.MOVE_SPEED
			elif self.velx < 0:
				self.velx = -self.MOVE_SPEED

	def flash_mode(self):
		if not self.consumed_bean:
			self.MOVE_SPEED = 10
		else:
			self.MOVE_SPEED = 3
		print 'starting flash'
		self.set_speed()
		pygame.time.set_timer(self.STOP_FLASH_EVENT, 300)
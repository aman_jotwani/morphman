import sys, pygame
from pygame.locals import *
import math

class Ball(pygame.sprite.Sprite):
	def __init__(self, x, y):
		pygame.sprite.Sprite.__init__(self)
		self.image = pygame.image.load("morph.png").convert()
		self.image.set_colorkey((255, 255, 255))
		self.rect = self.image.get_rect()
		self.rect.topleft = (x, y)
		self.x = self.rect.centerx
		self.y = self.rect.centery
		self.projection_velocity = 40
		self.projection_angle = math.radians(60)
		self.projected_x = x + 2*(self.projection_velocity**2)*math.sin(self.projection_angle)*math.cos(self.projection_angle)/9.8
		print self.projected_x
		self.h0 = y
		self.x0 = x
		self.t = 0

	def update(self):
		gravity = 9.8
		# print self.t
		tx = self.t/25.0
		self.x = self.x0 + self.projection_velocity*math.cos(self.projection_angle)*tx
		self.y = self.h0 - self.projection_velocity*math.sin(self.projection_angle)*tx + gravity*0.5*(tx**2)
		self.rect.center = (self.x, self.y)
		if self.y > 300:
			print self.x
			# sys.exit()
		# print self.x, self.y
		self.t += 1

pygame.init()
size = (600, 600)
window = pygame.display.set_mode(size)
ball = pygame.sprite.GroupSingle(Ball(300, 300))
reflect_ball = pygame.sprite.GroupSingle(Ball(ball.sprite.x, 300 + (300 - ball.sprite.y)))
game_timer = pygame.time.Clock()

while True:
	for event in pygame.event.get():
		if event.type == KEYDOWN and event.key == K_ESCAPE:
			sys.exit()
	game_timer.tick(120)
	ball.update()
	reflect_ball.sprite.rect.center = ball.sprite.rect.center
	window.fill((0, 255, 255))
	ball.draw(window)
	reflect_ball.draw(window)
	pygame.display.flip()